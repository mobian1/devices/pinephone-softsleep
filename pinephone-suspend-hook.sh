#!/bin/sh

prepare_suspend() {
	# Put modem in power saving mode
	mmcli -m 0 --command="AT+QSCLK=1"
	echo 1 > /sys/class/gpio/gpio358/value

	# Disable all CPUs except one
	echo 0 > /sys/bus/cpu/devices/cpu1/online
	echo 0 > /sys/bus/cpu/devices/cpu2/online
	echo 0 > /sys/bus/cpu/devices/cpu3/online

	# Lower CPU0 frequency
	echo "powersave" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor
	echo "Gone into sleep..."
}

resume_all() {
	echo "Waking up..."

	# Wake up modem
	echo 0 > /sys/class/gpio/gpio358/value
	mmcli -m 0 --command="AT+QSCLK=0"

	# Re-enable all CPUs
	echo 1 > /sys/bus/cpu/devices/cpu1/online
	echo 1 > /sys/bus/cpu/devices/cpu2/online
	echo 1 > /sys/bus/cpu/devices/cpu3/online

	# Restore CPU0 frequency
	echo "ondemand" > /sys/bus/cpu/devices/cpu0/cpufreq/scaling_governor
}

case $1 in
	pre) prepare_suspend ;;
	post) resume_all ;;
esac
